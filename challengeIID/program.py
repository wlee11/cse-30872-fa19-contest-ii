#!/usr/bin/env python3

import sys
import collections
from collections import Counter

maxHit = False
maxDepth = 0
word_list = []
index = 0
temp_index = 0

class Node():
    def __init__(self, value, vals):
        self.value = value
        self.vals = vals


def build(word, b):
    book = b.copy()
    vals = []

    index = 0
    for next_word in book:
        c = 0
        v = []
        if len(word) is len(next_word):
            for x in range(len(word)):
                if word[x] != next_word[x]:
                    c += 1
            if c is 1:
                vals.append(build(next_word, book[index:]))
        elif len(word) - len(next_word) == 1:
            temp_word = word
            for x in range(len(next_word)):
                if temp_word[x] != next_word[x] and temp_word[x+1] != next_word[x]:
                    c += 1
            if c is 0:
                vals.append(build(next_word, book[index:]))
        elif len(next_word) - len(word) == 1:
            temp_word = next_word
            for x in range(len(word)):
                if word[x] != temp_word[x] and temp_word[x+1] != word[x]:
                    c += 1
            if c is 0:
                vals.append(build(next_word, book[index:]))
        index += 1

    return Node(word, vals)


def depth_start(l):
    maxD = 0
    depth = 1
    for item in l.vals:
        depth_rec(item, depth+1)
    
    return maxD

def depth_rec(l, depth):
    global maxDepth
    global index
    global temp_index
    depth += 1
    if l.vals:
        for item in l.vals:
            depth_rec(item, depth)
            if maxDepth < depth:
                index = temp_index
                maxDepth = depth

    return

def dfs(s):
    global maxDepth
    global maxHit
    global word_list
    depth = maxDepth
    for item in s.vals:
        temp = dfs_rec(item, depth-1)
        if maxHit is True:
            word_list.append(item.value)
            break
    if maxHit is True:
        word_list.append(s.value)

    return depth

def dfs_rec(s, depth):
    global maxHit
    global maxDepth
    global word_list
    for item in s.vals:
        if maxHit is True:
            break
        temp = dfs_rec(item, depth-1)

        if depth == 2:
            maxHit = True
        if maxHit is True:
            word_list.append(item.value)
            break
    return depth 

if __name__== '__main__':
    wordTree = list()
    book  = list()
    maxDepth = 0
    for line in sys.stdin:
        book.append(line.strip())

    book = sorted(book)
    for i in range(len(book)):
        wordTree.append(build(book[i], book[i:]))

    # FIND MAX DEPTH THEN DFS UNTIL HIT DEPTH, RETURN STRINGS
    for item in wordTree:
        depth_start(item)
        temp_index += 1
    dfs(wordTree[index])
    word_list = word_list[::-1]
    print(maxDepth)
    for word in word_list:
        print(word)
