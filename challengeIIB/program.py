#!/usr/bin/env python3

import sys

path = [[]]

def makeOcean(x, y):
    #ocean = [[0 for _ in range(x)] for _ in range(y)]
    #ocean = [[0 for _ in range(y+1)]]
    ocean = []
    for _ in range(y):
        ocean.append([0] + list(map(int, sys.stdin.readline().split())))

    #ocean.append([0 for _ in range(y+1)])
    #print(ocean)
    #print("OCEAN:")
    #for row in ocean:
    #    print(row)

    #print()
    return ocean


def oceanPath(ocean, x, y):
    global path
    path = [[float("inf") for _ in range(x+1)] for _ in range(y)]

    if x == 1:
        minVal = float("inf")
        i = 0
        for row in range(1, y+1):
            if minVal > ocean[i][1]:
                minVal = ocean[i][1]
                minRow = i
            i += 1
        print(minVal)
        print(minRow+1)
    elif y > 2:
        i = 0
        for row in ocean:
            path[i][1] = ocean[i][1]
            path_rec(i, 1, x, y)
            i += 1

        strace = [str(a) for a in trace(ocean, x, y)]
        #print(path[y-1][x])
        print(' '.join(strace[::-1]))
            
    elif y == 2:
        i = 0
        for row in ocean:
            path[i][1] = ocean[i][1]
            path2_rec(i, 1, x, y)
            i+= 1
        strace = [str(a) for a in trace2(ocean, x, y)]
        #print(path[y-1][x])
        print(' '.join(strace[::-1]))
    elif y == 1:
        val = 0
        path = []
        for l in ocean:
            for n in l:
                val += n
                path += ['1']
        path = path[1:]
        print(val)
        print(' '.join(path))
    
    #for row in path:
    #    print(row)
    #print()


def path2_rec(row, col, x, y):
    global path

    if col >= x:
        return

    # Two scenarios - either in top or bottom
    if (path[0][col+1] - ocean[0][col+1]) > path[row][col]:
        path[0][col+1] = ocean[0][col+1] + path[row][col]
        path_rec(0, col+1, x, y)
    if (path[1][col+1] - ocean[1][col+1]) > path[row][col]:
        path[1][col+1] = ocean[1][col+1] + path[row][col]
        path_rec(1, col+1, x, y)


def path_rec(row, col, x, y):
    global path

    if col == x:
        return

    # Check for various scenarios
    if row is 0:
        if (path[y-1][col+1] - ocean[y-1][col+1]) > path[row][col]:
            path[y-1][col+1] = ocean[y-1][col+1] + path[row][col]
            path_rec(y-1, col+1, x, y)
        if (path[row][col+1] - ocean[row][col+1]) > path[row][col]:
            path[row][col+1] = ocean[row][col+1] + path[row][col]
            path_rec(row, col+1, x, y)
        if (path[row+1][col+1] - ocean[row+1][col+1]) > path[row][col]:
            path[row+1][col+1] = ocean[row+1][col+1] + path[row][col]
            path_rec(row+1, col+1, x, y)
    elif row is (y-1):
        if (path[row-1][col+1] - ocean[row-1][col+1]) > path[row][col]:
            path[row-1][col+1] = ocean[row-1][col+1] + path[row][col]
            path_rec(row-1, col+1, x, y)
        if (path[row][col+1] - ocean[row][col+1]) > path[row][col]:
            path[row][col+1] = ocean[row][col+1] + path[row][col]
            path_rec(row, col+1, x, y)
        if (path[0][col+1] - ocean[0][col+1]) > path[row][col]:
            path[0][col+1] = ocean[0][col+1] + path[row][col]
            path_rec(0, col+1, x, y)
    else:
        if (path[row-1][col+1] - ocean[row-1][col+1]) > path[row][col]:
            path[row-1][col+1] = ocean[row-1][col+1] + path[row][col]
            path_rec(row-1, col+1, x, y)
        if (path[row][col+1] - ocean[row][col+1]) > path[row][col]:
            path[row][col+1] = ocean[row][col+1] + path[row][col]
            path_rec(row, col+1, x, y)
        if (path[row+1][col+1] - ocean[row+1][col+1]) > path[row][col]:
            path[row+1][col+1] = ocean[row+1][col+1] + path[row][col]
            path_rec(row+1, col+1, x, y)


def trace(ocean, x, y):
    global path
    pathList = []
    col = x

    minVal = float("inf")
    for i in range(1, y+1):
        if minVal > path[i-1][x]:
            minVal = path[i-1][x]
            row = i - 1


    pathList.append(row+1)
    print(path[row][col])

    while col != 1:
        #pathList.append(row + 1)
        if row is 0:
            minimum = min( path[row][col-1], path[row+1][col-1], path[y-1][col-1] )
            if minimum == path[row][col-1]:
                pathList.append(row+1)
            elif minimum == path[row+1][col-1]:
                pathList.append(row+2)
                row += 1
            elif minimum == path[y-1][col-1]:
                pathList.append(y)
                row = y-1
        elif row is (y-1):
            minimum = min( path[row-1][col-1], path[row][col-1], path[0][col-1] )
            if minimum == path[0][col-1]:
                pathList.append(1)
                row = 0
            elif minimum == path[row-1][col-1]:
                pathList.append(row)
                row -= 1
            elif minimum == path[row][col-1]:
                pathList.append(row+1)
        else:
            minimum = min( path[row-1][col-1], path[row][col-1], path[row+1][col-1] )
            if minimum == path[row-1][col-1]:
                pathList.append(row)
                row -= 1
            elif minimum == path[row][col-1]:
                pathList.append(row+1)
            elif minimum == path[row+1][col-1]:
                pathList.append(row+2)
                row += 1

        col -= 1
    return pathList

def trace2(ocean, x, y):
    global path
    pathList = []
    col = x

    minVal = float("inf")
    for i in range(1, y+1):
        if minVal > path[i-1][x]:
            minVal = path[i-1][x]
            row = i-1

    print(path[row][col])
    pathList.append(row+1)
    while col != 1:
        minimum = min(path[0][col-1], path[1][col-1])
        if minimum == path[0][col-1]:
            pathList.append(1)
            row = 0
        else:
            pathList.append(2)
            row = 1
        col -= 1

    return pathList


if __name__=='__main__':
    while True:
        try:
            y, x = map(int, sys.stdin.readline().strip().split())
        except ValueError:
            break
        #print(x, y)
        ocean = makeOcean(x, y)
        oceanPath(ocean, x, y)
