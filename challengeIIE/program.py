#!/usr/bin/env python3

import sys

f = []

def find(tree, i, path, tar, tot):
	tot += tree[i]

	kid = True
	if len(tree) > i*2 + 2 and tree[i*2 + 2] != 0:
		kid = False
	if len(tree) > i*2 + 1 and tree[i*2 + 1] != 0:
		kid = False

	if kid:
		if tot == tar:
			global f
			new = list(path)
			new.append(tree[i])
			f.append(new)
		return


	if len(tree) > i*2 + 1 and tree[i*2 + 1] != 0:
		new = list(path)
		new.append(tree[i])
		find(tree, i*2 + 1, new, tar, tot)
	if len(tree) > i*2 + 2 and tree[i*2 + 2] != 0:
		new = list(path)
		new.append(tree[i])
		find(tree, i*2 + 2, new, tar, tot)

	return

	
for line in sys.stdin:
	target = line.strip()
	tree = list(map(int, sys.stdin.readline().strip().split()))
	path = []
	
	find(tree, 0, path, int(target), 0)


	f.sort()
	
	for p in f:
		print("{}: {}".format(target, ", ".join(list(map(str,p)))))

	f = []
